#include <stdio.h>  //incluimos la libreria de estandar input/output
#include <unistd.h> //para hacer sleep
#include <stdlib.h> //para libreria de numeros random: srand, rand
#include <time.h>   //para inicializar el tiempo
#include <sys/wait.h>

void do_nothing(int microseconds, char *mensaje)
{
  usleep(microseconds); //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n", mensaje);
}

void do_nothing_random(char *mensaje)
{
  srand(time(NULL));                    //inicializar la semilla del generador random:
  int microseconds = rand() % 1000 + 1; //generar un numer random entre 1 y 1000:
  usleep(microseconds);                 //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n", mensaje);
}

int main()
{
  char *msj = "Sin fork";

  pid_t pid;

  printf("----------------------------------------------\n");
  printf("Inicia Procesos SIN Fork\n");
  printf("0 segundos\n");
  do_nothing(2000000, msj);
  printf("do_nothing terminado\n");
  printf("2 segundos\n");
  do_nothing(2000000, msj);
  printf("do_nothing terminado\n");
  printf("4 segundos\n");

  printf("----------------------------------------------\n");
  printf("Inicia Procesos CON Fork\n");
  msj = "Con fork";

  printf("0 segundos\n");

  //por cada fork, se crea 1 proceso mas.
  pid = fork(); // si el pid es 0, es el proceso hijo, si el pid es -1 es un error, y si es >0 entonces es el proceso padre.

  //CORREN EN PARALELO
  if (pid == 0)
  {
    do_nothing(2000000, msj);
    printf("2 segundos, soy el hijo\n");
  }
  if (pid > 0)
  {
    do_nothing(2000000, msj);
    printf("2 segundos, soy el padre\n");
  }
  printf("do_nothing terminado\n");

  msj = "Con fork y wait";

  wait(NULL);
  if (pid == 0)
  {
    printf("----------------------------------------------\n");
    printf("Inicia Procesos CON Fork y WAIT\n");
    printf("0 segundos\n");
    do_nothing(2000000, msj);
    printf("2 segundos, soy el hijo\n");
  }
  else if (pid > 0)
  {
    do_nothing(2000000, msj);
    printf("4 segundos, soy el padre\n");
  }
  printf("do_nothing terminado\n");

  return 0;
}

//para compilar: gcc do_nothing.c -o ejecutable
//para ejecutar: ./ejecutable
