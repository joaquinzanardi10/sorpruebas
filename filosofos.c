#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>

#include <stdlib.h>
#include <unistd.h> //para hacer sleep
#include <time.h>   //para inicializar el tiempo

int palillos = 5;
int comer;

pthread_mutex_t mutexG;

void do_nothing(int microseconds, char *mensaje)
{
    usleep(microseconds); //dormir el thread, simula que esta haciendo alguna tarea
    printf("\n %s \n\n", mensaje);
}

void do_nothing_random(char *mensaje)
{
  srand(time(NULL));                    //inicializar la semilla del generador random:
  int microseconds = rand() % 3000000 + 1000000; //generar un numer random entre 1 y 1000:
  usleep(microseconds);                 //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n", mensaje);
}

void fil(int n){
    if(comer<1) exit(0);
        int i = n;
        //Estado Pensando
        printf("“Soy el Filósofo %d Pensando”\n", i);
        do_nothing_random("");
        //Estado Agarrando palillo}

        pthread_mutex_lock(&mutexG);
        
        if (palillos >= 2 && comer > 0)
        {
            printf("---Inicio Seccion critica---\n");
            printf("“Soy el Filósofo %d : Agarrando palillo izquierdo”\n", i);
            printf("“Soy el Filósofo %d : Agarrando palillo derecho”\n", i);
            palillos -= 2;
            comer--;
            printf("---Fin Seccion critica---\n\n");
            pthread_mutex_unlock(&mutexG);
            

            printf("“Palillos disponibles: %d”\n", palillos);
            //Estado Comiendo
            printf("“Soy el Filósofo %d : Comiendo”\n", i);
            
            do_nothing_random("");
            palillos+=2;
            printf("Filosofo %d: Suelto palillos: ",i);
        }else{
            pthread_mutex_unlock(&mutexG);
        }
}

static void *filosofo_1()
{
    while (1)
    {
        fil(1);
    }
}

static void *filosofo_2()
{
    while (1)
    {
        fil(2);
    }
}

static void *filosofo_3()
{
    while (1)
    {
        fil(3);
    }
}

static void *filosofo_4()
{
    while (1)
    {
        fil(4);
    }
}

static void *filosofo_5()
{
    while (1)
    {
        fil(5);
    }
}

int main(void)
{
    printf("Ingrese cuantos platos se pueden comer:\n");
    scanf("%d",&comer);
    //DEFINE LAS VARIABLES DEL TIPO THREAD
    pthread_t thread_1, thread_2, thread_3, thread_4, thread_5;

    //INICIA LOS SEMAFOROS

    //crea los hilos (creo)
    pthread_create(&thread_1, NULL, *filosofo_1, NULL);
    pthread_create(&thread_2, NULL, *filosofo_2, NULL);
    pthread_create(&thread_3, NULL, *filosofo_3, NULL);
    pthread_create(&thread_4, NULL, *filosofo_4, NULL);
    pthread_create(&thread_5, NULL, *filosofo_5, NULL);

    //no se...
    pthread_join(thread_1, NULL);
    pthread_join(thread_2, NULL);
    pthread_join(thread_3, NULL);
    pthread_join(thread_4, NULL);
    pthread_join(thread_5, NULL);

    pthread_exit(NULL);
    return 0;
}

//EJECUCION:
//gcc filosofos.c -o filosofos -lpthread && ./filosofos