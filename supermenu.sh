#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
red=$(tput setaf 1)
green=$(tput setaf 2)
blue=$(tput setaf 4)
bg_blue=$(tput setab 4)
reset=$(tput sgr0)
bold=$(tput setaf bold)

#------------------------------------------------------
# TODO: Completar con su path
#------------------------------------------------------
proyectoActual="${PWD}"
updateRemote=$(
    cd $proyectoActual
    git remote update
)

updateStatus=$(
    cd $proyectoActual
    git status
)

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu() {
    imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U "

    echo -e "\t\t El proyecto actual esta en:"
    echo -e "\t\t $proyectoActual"

    echo -e "\t\t"
    echo -e "\t\t Opciones:"
    echo ""
    echo -e "\t\t\t a.  Ver estado del proyecto"
    echo -e "\t\t\t b.  Guardar cambios"
    echo -e "\t\t\t c.  Actualizar repo"
    echo -e "\t\t\t f.  Abrir en terminal"
    echo -e "\t\t\t g.  Abrir en carpeta"
    echo ""
    echo -e "\t\t\t --- Ejercicios Extra ---"
    echo ""
    echo -e "\t\t\t h.  Do Nothing - Fork"
    echo -e "\t\t\t i.  Filosofos Chinos - Exclusion Mutua"
    echo -e "\t\t\t j.  Pienso luego existo - Sincronizacion"

    echo -e "\t\t\t q.  Salir"
    echo ""
    determinarUpdate
    determinarStatus
    echo -e "Escriba la opción y presione ENTER"
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado() {
    clear
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "$(date +"%d-%m-%Y %T")\t\t\t\t\t USERNAME:$USER"
    echo ""
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}"
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}"
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}"
    echo ""
}

esperar() {
    echo ""
    echo -e "Presione enter para continuar"
    read ENTER
}

malaEleccion() {
    echo -e "Selección Inválida ..."
}

decidir() {
    echo $1
    while true; do
        echo "desea ejecutar? (s/n)"
        read respuesta
        case $respuesta in
        [Nn]*) break ;;
        [Ss]*)
            eval $1
            break
            ;;
        *) echo "Por favor tipear S/s ó N/n." ;;
        esac
    done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion() {
    imprimir_encabezado "\tOpción a.  Ver estado del proyecto"
    echo "---------------------------"
    echo "Somthing to commit?"
    decidir "cd $proyectoActual; git status"

    echo "---------------------------"
    echo "Incoming changes (need a pull)?"
    decidir "cd $proyectoActual; git fetch origin"
    decidir "cd $proyectoActual; git log HEAD..origin/master --oneline"
}

b_funcion() {
    imprimir_encabezado "\tOpción b.  Guardar cambios"
    decidir "cd $proyectoActual; git add -A"
    echo "Ingrese mensaje para el commit:"
    read mensaje
    decidir "cd $proyectoActual; git commit -m \"$mensaje\""
    decidir "cd $proyectoActual; git push"
}

c_funcion() {
    imprimir_encabezado "\tOpción c.  Actualizar repo"
    decidir "cd $proyectoActual; git pull"
}

f_funcion() {
    imprimir_encabezado "\tOpción f.  Abrir en terminal"
    decidir "cd $proyectoActual; xterm &"
}

g_funcion() {
    imprimir_encabezado "\tOpción g.  Abrir en carpeta"
    decidir "gnome-open $proyectoActual &"
}

h_funcion() {
    decidir "clear && gcc do_nothing.c -o do && time ./do"
}

i_funcion() {
    decidir "clear && gcc filosofos.c -o filosofos -lpthread && ./filosofos"
}

j_funcion() {
    decidir "clear && gcc Sincronizacion.c -o Sincronizacion -lpthread && ./Sincronizacion"
}

#------------------------------------------------------
# TODO: Completar con el resto de ejercicios del TP, una funcion por cada item
#------------------------------------------------------
revisarEstado() {
    imprimir_encabezado "\tEstado del proyecto"
    echo "---------------------------"
    decidir "cd $proyectoActual; git status"

    echo "---------------------------"
    echo "Incoming changes (need a pull)?"
    decidir "cd $proyectoActual; git fetch origin"
    decidir "cd $proyectoActual; git log HEAD..origin/master --oneline"
}

determinarUpdate() {
    updateStatus=$(cd $proyectoActual && git status)
    msj=$(echo $updateStatus | grep -o "Tu rama está detrás de")

    if [[ $msj == "" ]]; then
        msj=$(echo $updateStatus | grep -o "Your branch is behind")
    fi

    #echo $msj alerta
    if [[ $msj == "Tu rama está detrás de" || $msj == "Your branch is behind" ]]; then
        msj=${bold}${red}"DESINCRONIZADO - ACTUALIZAR REPO \n${reset}"
    else
        msj=${bold}${green}"SINCRONIZADO CON REPO\n${reset}"
    fi
    echo -e $msj
}

determinarStatus() {
    msj=$(echo $updateStatus | grep -o "nada para hacer commit")

    if [[ $msj == "" ]]; then
        msj=$(echo $updateStatus | grep -o "nothing to commit")
    fi

    #echo $msj alerta
    if [[ $msj == "nada para hacer commit" || $msj == "nothing to commit" ]]; then
        msj=${bold}${green}"SIN CAMBIOS LOCALES\n${reset}"
    else
        msj=${bold}${red}"ACTUALIZACION LOCAL - GUARDAR CAMBIOS \n${reset}"
    fi
    echo -e $msj
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while true; do
    # 1. mostrar el menu
    imprimir_menu
    # 2. leer la opcion del usuario
    read opcion

    case $opcion in
    a | A) a_funcion ;;
    b | B)
        b_funcion
        updateStatus=$(
            cd $proyectoActual
            git status
        )
        ;;
    c | C)
        c_funcion
        updateRemote=$(
            cd $proyectoActual
            git remote update
        )
        ;;
    d | D) d_funcion ;;
    e | E) e_funcion ;;
    f | F) f_funcion ;;
    g | G) g_funcion ;;
    h | H) h_funcion ;;
    i | I) i_funcion ;;
    j | J) j_funcion ;;
    q | Q) break ;;
    *) malaEleccion ;;
    esac
    esperar
done
