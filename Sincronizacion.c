#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h> //para hacer sleep
#include <time.h>   //para inicializar el tiempo
#include <sys/wait.h>

int contador, max;

sem_t pienso;
sem_t lavoPlatos;
sem_t limpioPiso;
sem_t riegoPlantas;
sem_t existo;
sem_t hablar;
sem_t tomarDecision;


sem_t aux_2;
sem_t aux_3;
sem_t aux_4;


sem_t aux_6;
sem_t aux_7;

/*
Pienso

Mientras lavo los platos,
Mientras limpio el piso,
Mientras riego las plantas

Existo!
Hablar, Tomar una decisión
*/

void do_nothing(int microseconds, char *mensaje)
{
  usleep(microseconds); //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n", mensaje);
}

static void *imprimir_pienso()
{
    while (1)
    {
        
        
        sem_wait(&aux_6);
        sem_wait(&aux_7);
        sem_wait(&pienso);
        contador--;
        if (contador < 0) exit(0);
        printf("\n%d)\nPienso\n", max-contador);
        sem_post(&lavoPlatos);
        sem_post(&limpioPiso);
        sem_post(&riegoPlantas);
        sem_post(&existo);
    }
}

static void *imprimir_lavoPlatos()
{
    while (1)
    {
        sem_wait(&lavoPlatos);
        printf("Mientras lavo los platos, ");
        sem_post(&aux_2);
    }
}

static void *imprimir_limpioPiso()
{
    while (1)
    {
        sem_wait(&limpioPiso);
        printf("Mientras limpio el piso, ");
        sem_post(&aux_3);
    }
}

static void *imprimir_riegoPlantas()
{
    while (1)
    {
        sem_wait(&riegoPlantas);
        printf("Mientras riego las plantas, ");
        sem_post(&aux_4);
    }
}

static void *imprimir_existo()
{
    while (1)
    {        
        sem_wait(&existo);
        sem_wait(&aux_2);
        sem_wait(&aux_3);
        sem_wait(&aux_4);
        printf("\nExisto!\n");
        sem_post(&hablar);
        sem_post(&tomarDecision);
        sem_post(&pienso);  //si sacamos esto no imprimer hablar ni tomar decision aunque le hagamos post a los dos
    }
}

static void *imprimir_hablar()
{
    while (1)
    {
        sem_wait(&hablar);
        printf("Hablar, ");
        do_nothing(2000,"");
        sem_post(&aux_6);
    }
}

static void *imprimir_tomarDecision()
{
    while (1)
    {
        sem_wait(&tomarDecision);
        printf("Tomar una decision, ");
        sem_post(&aux_7);
    }
}

int main(void)
{
    printf("Ingrese cantidad de repeticiones:\n");
    scanf("%d", &contador);
    max = contador;

    pthread_t thread_1, thread_2, thread_3, thread_4, thread_5, thread_6, thread_7;

    sem_init(&pienso, 0, 1);
    sem_init(&lavoPlatos, 0, 0);
    sem_init(&limpioPiso, 0, 0);
    sem_init(&riegoPlantas, 0, 0);
    sem_init(&existo, 0, 0);
    sem_init(&hablar, 0, 0);
    sem_init(&tomarDecision, 0, 0);

    sem_init(&aux_2, 0, 0);
    sem_init(&aux_3, 0, 0);
    sem_init(&aux_4, 0, 0);

    sem_init(&aux_6, 0, 1);
    sem_init(&aux_7, 0, 1);


    pthread_create(&thread_1, NULL, *imprimir_pienso, NULL);
    pthread_create(&thread_2, NULL, *imprimir_lavoPlatos, NULL);
    pthread_create(&thread_3, NULL, *imprimir_limpioPiso, NULL);
    pthread_create(&thread_4, NULL, *imprimir_riegoPlantas, NULL);
    pthread_create(&thread_5, NULL, *imprimir_existo, NULL);
    pthread_create(&thread_6, NULL, *imprimir_hablar, NULL);
    pthread_create(&thread_7, NULL, *imprimir_tomarDecision, NULL);

    pthread_join(thread_1, NULL);
    pthread_join(thread_2, NULL);
    pthread_join(thread_3, NULL);
    pthread_join(thread_4, NULL);
    pthread_join(thread_5, NULL);
    pthread_join(thread_6, NULL);
    pthread_join(thread_7, NULL);

    sem_destroy(&pienso);
    sem_destroy(&lavoPlatos);
    sem_destroy(&limpioPiso);
    sem_destroy(&riegoPlantas);
    sem_destroy(&existo);
    sem_destroy(&hablar);
    sem_destroy(&tomarDecision);

    sem_destroy(&aux_2);
    sem_destroy(&aux_3);
    sem_destroy(&aux_4);   

    sem_destroy(&aux_6);
    sem_destroy(&aux_7);

    pthread_exit(NULL);
    
    return 0;
}

//EJECUCION:
//gcc Sincronizacion.c -o Sincronizacion -lpthread && ./Sincronizacion